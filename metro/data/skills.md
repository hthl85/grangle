*	_2 years_ working experience in developing window and web applications.   
*	_1-year_ experience with database management using **MSSQL**, **MySQL**, and **Oracle**.   
*	Knowledge in different developing languages: **C++, C#, Java, PHP, ASP, HTML, XML, JavaScript, jQuery, AngularJS, SQL, CSS, Markdown...**   
*	Working with different developing tools: **Eclipse, MS Visual Studio, Netbeans, XCode, Visual Source Safe, TortoiseSVN, SQLyog.**   
*	Willingness to work independently as well as in a team.   
    *	Have worked in team for many projects.   
    *	Have done some projects as a freelancer.   
*	Ambitious and motivated with a strong work ethic.   