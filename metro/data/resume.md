---
###Skills & Qualifications###

*	_2 years_ working experience in developing window and web applications.
*	_1-year_ experience with database management using **MSSQL**, **MySQL**, and **Oracle**.
*	Knowledge in different developing languages: **C++, C#, Java, PHP, ASP, HTML, XML, JavaScript, jQuery, AngularJS, SQL, CSS, Markdown,etc.**
*	Working with different developing tools: **Eclipse, MS Visual Studio, Netbeans, XCode, Visual Source Safe, TortoiseSVN, SQLyog.**
*	Willingness to work independently as well as in a team.
    *	Have worked in team for many projects.
    *	Have done some projects as a freelancer.
*	Ambitious and motivated with a strong work ethic.

---
###Working experiences###

*	__Bioinformatic Research Assistant at Research Department of The Princess Margaret Cancer Centre/Ontario Cancer Institute ( Sep, 2014 - Dec, 2014)__
*	__Bioinformatic Research Assistant at Research Department of The Princess Margaret Cancer Centre/Ontario Cancer Institute ( Jan, 2014 - Apr, 2014)__
*   __Web developer at RAW Integrated Inc. (Apr, 2013 - Aug, 2013)__
    *   Fixed bugs and maintained many projects.
    *   Co-operated with two developers and one tester to develop a new project that manages user accounts, orders, and shipping transactions, interact with client's system by reading and generating files from/to FTP server within a month.
    *   Environment: PHP-MVC using Zend Framework, MySQL, Apache.
*   __Customer service at Blue Bin Unlimited. (Jun, 2010 - Mar, 2012)__
    *   Prepared, refunded, and exchanged customer orders.
    *   Organized and cataloged warehouse's stocks.
    *   Created paperwork and solved customer's problem.
*   __Web developer at 4th Dimension Inc. (May, 2010)__
    *   Maintained website [www.contractors.com](http://www.contractors.com) individually.
    *   Environment: Using web service .Net, Google Map API, MySQL, MSSQL, IIS and Apache.

---
###Education###

*   __Candidate of Bachelor of Computer Science, Honors Bioinfomatics, Cooperative Program, University of Waterloo - Waterloo, ON (Sep, 2012 - present).__

---
###Awards###
*	__Term Dean's Honor University of Waterloo (Winter term, 2013)__
*	__Term Dean's Honor University of Waterloo ( Fall term, 2013)__
    *	_Award for student who achieves an overall term average of at least 80%_
*	__University of Waterloo President's Scholarship of Distinction (Sep, 2012).__
    *	_Scholarship for student who achieves average above 95% in high school._
*	__University of Waterloo Entrance Scholarship (Sep, 2012).__
*	__The Governor General's Academic Medal (Jun, 2012).__
    *	_The Bronze Medal - for student who achieves the highest average upon graduating from a secondary school._


