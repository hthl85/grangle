*   Thank __Sara Soueidan__ for the [_"Window 8 like animation"_](http://sarasoueidan.com/blog/windows8-animations/).
*   This website also uses free icons from [_Icomoon_](https://icomoon.io/) and some images
    designed by [_Freepik_](http://www.freepik.com/free-vector/science-and-biology-scribbles_761073.htm).

