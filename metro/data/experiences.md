*	__Bioinformatic Research Assistant at Research Department of The Princess Margaret Cancer Centre/Ontario Cancer Institute ( Sep, 2014 - Dec, 2014)__
*	__Bioinformatic Research Assistant at Research Department of The Princess Margaret Cancer Centre/Ontario Cancer Institute ( Jan, 2014 - Apr, 2014)__
*   __Web developer at RAW Integrated Inc. (Apr, 2013 - Aug, 2013)__
    *   Fixed bugs and maintained many projects.
    *   Co-operated with two developers and one tester to develop a new project that manages user accounts, orders, and shipping transactions, interact with client's system by reading and generating files from/to FTP server within a month.
    *   Environment: PHP-MVC using Zend Framework, MySQL, Apache.
*   __Customer service at Blue Bin Unlimited. (Jun, 2010 - Mar, 2012)__
    *   Prepared, refunded, and exchanged customer orders.
    *   Organized and cataloged warehouse's stocks.
    *   Created paperwork and solved customer's problem.
*   __Web developer at 4th Dimension Inc. (May, 2010)__
    *   Maintained website [www.contractors.com](http://www.contractors.com) individually.
    *   Environment: Using web service .Net, Google Map API, MySQL, MSSQL, IIS and Apache.

