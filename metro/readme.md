#  Personal website - [letrong.ca](http://www.letrong.ca) #

---

## Info ##
This is a new design of letrong.ca. I made this website in order to prepare for my next work term. Basicly, this website just shows my resume, and link to my social hub and some of my presonal projects. 

---

## Language ##

* CSS3      
* HTML5   
* jQuery   
* Markdown   

---

## Credit ##

* Thank Sara Soueidan for the "Window 8 like animation".   
* This website also uses free icons from Icomoon and some images designed by Freepik.   


