(function($) {
    $.fn.rotation = function(options) {
        //Default settings
        var settings = $.extend({
            angle: -360,
            time: 10000
        }, options);

        this.animate({
            transform: settings.angle}, {
            step: function(angle) {
                var styles = {
                    "transform": "rotate(" + angle + "deg)",
                    "-ms-transform": "rotate(" + angle + "deg)",
                    "-webkit-transform": "rotate(" + angle + "deg)"
                };
                $(this).css(styles);
            },
            easing: "linear",
            duration: settings.time});
        //debug(this);
    };

    // Private function for debugging.
    function debug($obj) {
        if (window.console && window.console.log) {
            window.console.log($obj);
        }
    };
})(jQuery);