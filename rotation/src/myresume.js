(function($) {
    $(document).ready(function() {
        $('.div-resume-btn').click(function() {
            var clickId = this.id;
            var visibleId = $('.resume-detail > div:visible').attr('id');
            if(clickId != visibleId){
                $('.resume-detail #' + visibleId).toggle('inactive');
                $('.resume-detail #' + clickId).toggle('active');
                $('html,body').animate({ scrollTop: $('#resume-wrapper').offset().top }, 'slow');
            }
        });
    });

})(jQuery);