function pullData(divId, filePath){
	var mdHttp;
	if (window.XMLHttpRequest){
		// code for IE7+, Firefox, Chrome, Opera, Safari
	  	mdHttp=new XMLHttpRequest();
	}
	else{
		// code for IE6, IE5
	  	mdHttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	mdHttp.onreadystatechange=function(){
	  	if (mdHttp.readyState==4 && mdHttp.status==200){
	    	var mdText = mdHttp.responseText;
	    	$(divId).innerHTML = markdown.toHTML(mdText);
	    }
	}
	
	mdHttp.open("GET", filePath, true);
	mdHttp.overrideMimeType("text/plain");
	mdHttp.send();
}

var $ = function (id) { return document.getElementById(id); };