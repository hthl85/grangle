*	__Term Dean's Honor University of Waterloo (Winter term, 2013)__
*	__Term Dean's Honor University of Waterloo ( Fall term, 2013)__
    *	_Award for student who achieves an overall term average of at least 80%_
*	__University of Waterloo President's Scholarship of Distinction (Sep, 2012).__
    *	_Scholarship for student who achieves average above 95% in high school._
*	__University of Waterloo Entrance Scholarship (Sep, 2012).__
*	__The Governor General's Academic Medal (Jun, 2012).__
    *	_The Bronze Medal - for student who achieves the highest average upon graduating from a secondary school._

