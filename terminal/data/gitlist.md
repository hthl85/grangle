Le Trong's projects
===================

---
+	__[letrong.git](http://gitlist.letrong.ca/letrong.git/):__ 
	+	Description: My personal website repository
	+	Language: _HTML5, CSS, JavaScript, Markdown_
+	__[grangle.git](http://gitlist.letrong.ca/grangle.git/):__
	+	Description: My RESTful API repository
	+	Language: _MongoDB, Express.js, Angular.js, Node.js_
+	__[arduino.git](http://gitlist.letrong.ca/arduino.git/):__
	+	Description: My arduino project
	+	Language: _C_
+	__[firefox.git](http://gitlist.letrong.ca/firefox.git/):__
	+	Description: My Firefox add-ons project
	+	Language: _JavaScript_
+	__[google.git](http://gitlist.letrong.ca/google.git/):__
	+	Description: My Chrome extensions project
	+	Language: _JavaScript_
+	__[uwass.git](http://gitlist.letrong.ca/uwass.git/):__
	+	Description: My pass assignments
	+	Language: _C, C++, Bash_