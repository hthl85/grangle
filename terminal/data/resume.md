Le Trong's resumé
=================

---
###Skills & Qualifications###

*	__2 years__ working experience in developing window and web applications.
*	__1-year__ experience with database management using _MSSQL_, _MySQL_, and _Oracle_.
*	Knowledge in different developing languages: _C++, C#, PHP, ASP, HTML, XML, JavaScript, jQuery, AngularJS, NodeJS, SQL, CSS, Markdown, etc._
*	Working with different developing tools: _Eclipse, MS Visual Studio, Netbeans, TortoiseSVN, Git, SQLyog._
*	Willingness to work independently as well as in a team.
    *	Have worked in team for many projects.
    *	Have done some projects as a freelancer.
*	Ambitious and motivated with a strong work ethic.

---
###Working experiences###

*   __.NET developer at CrossChasm Technologies Inc__ (May, 2015 - Aug, 2015)
    *   Fixed bugs and maintained many projects.
    *   Tested and QA other developers' tasks.
    *   __Environment__: _.NET MVC3, MS Visual Studio, MSSQL, Azure._
*	__Bioinformatic Research Assistant at Research Department of The Princess Margaret Cancer Centre/Ontario Cancer Institute__ (Sep, 2014 - Dec, 2014)
    *   Tested new bioinformatics tools.
    *   Analyzed research data using R Studio.
    *   Fixed bugs and maintained different bioinformatics tools.
    *   __Environment__: _Unix, R studio._
*	__Bioinformatic Research Assistant at Research Department of The Princess Margaret Cancer Centre/Ontario Cancer Institute__ (Jan, 2014 - Apr, 2014)
    *   Developed a website for cancer research campaign, and some intranet website for team members.
    *   Fixed bugs and maintained different bioinformatics tools.
    *   Worked with network administrator to build a new cluster.
    *   __Environment__: _Unix, CMS using Drupal, and Joomla._
*   __Web developer at RAW Integrated Inc__ (Apr, 2013 - Aug, 2013)
    *   Fixed bugs and maintained many projects.
    *   Co-operated with two developers and one tester to develop a new project that manages user accounts, orders, and shipping transactions, interact with client's system by reading and generating files from/to FTP server within a month.
    *   __Environment__: _PHP-MVC using Zend Framework, MySQL, Apache._


---
###Education###

*   __Candidate of Bachelor of Computer Science, Honors Bioinfomatics, Cooperative Program, University of Waterloo - Waterloo, ON__ (Sep, 2012 - present).

---
###Awards###
*	_Term Dean's Honor University of Waterloo_ (Winter term, 2013)
*	_Term Dean's Honor University of Waterloo_ (Fall term, 2013)
    *	Award for student who achieves an overall term average of at least 80%
*	_University of Waterloo President's Scholarship of Distinction_ (Sep, 2012).
    *	Scholarship for student who achieves average above 95% in high school.
*	_University of Waterloo Entrance Scholarship_ (Sep, 2012).
*	_The Governor General's Academic Medal_ (Jun, 2012).
    *	The Bronze Medal - for student who achieves the highest average upon graduating from a secondary school.

---

