(function () {
    "use strict"
    var counter = 0;
    var isFirst = true;

    var runCommand = function (command) {
        var commands = command.split(' ');
        if (commands[0] === "help" && commands.length === 1) {
            getData("data/help.md");
        } else if (commands[0] === "clear" && commands.length === 1) {
            $(".history").html("");
            $("#command-input").val("");
        } else if (commands[0] === "resume" && commands.length === 1) {
            getData("data/resume.md");
        } else if (commands[0] === "git") {
            if (commands.length === 2 && commands[1] === "list") {
                getData("data/gitlist.md");
            } else {
                console.log("Invalid command");
            }
        } else if (commands[0] === "song" && commands.length === 1) {
            getData("data/song.md");
        } else {
            console.log("Invalid command");
        }
    };

    var getData = function (url) {
        $.get(url, function (data) {
            var tmpId = "cts" + counter++;
            var textAppended = "<span class='domain' id='" + tmpId +
                "'>letrong.ca > </span> <span class='oldCommand'>" +
                $("#command-input").val() + "</span> <br/>";
            $(".history").append(textAppended);
            $(".history").append(markdown.toHTML(data) + "<br/>");
            $("#command-input").val("");
            $('html,body').animate({
                scrollTop: $('#' + tmpId).offset().top
            }, 'slow');
        }, "text");
    };

    $("#command-input").bind("enterKey", function (e) {
        runCommand($("#command-input").val());
    });

    $("#command-input").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");
        }
    });

    $(".close").click(function (event) {
        $("#greetingDialog").removeClass("turnon").addClass("turnoff");
        $("#command-input").focus();
    });

    $(".open").click(function (event) {
        $("#greetingDialog").removeClass("hideout").removeClass("turnoff").addClass("turnon");
    });
})();